import React, { useContext, useState, useEffect } from 'react';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';

import UserContext from '../UserContext';



export default function Courses() {

	const { user } = useContext(UserContext);

	const[allCourses, setAllCourses] = useState([]);

	const fetchData = () => {
		fetch('https://b165-shared-api.herokuapp.com/courses/')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllCourses(data)
		})
	}

	useEffect(() => {
		fetchData()
	}, [])

	return (
		<>
			{(user.isAdmin === true) ? 

				<AdminView coursesData={allCourses} fetchData={fetchData}/>

				:

				<UserView coursesData={allCourses}/>

			}
		</>

		)
}